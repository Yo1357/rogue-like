﻿using UnityEngine;
using System.Collections;

public class Switching : MonoBehaviour {

    public GameObject playerRed;
    public GameObject playerGreen;

    public void PlayerRed()
    {
        playerGreen.SetActive(false);
        playerRed.SetActive(true);
    }

    public void PlayerGreen()
    {
        playerRed.SetActive(false);
        playerGreen.SetActive(true);
    }
}
