﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class CharactersCreation : MonoBehaviour {

    private List<GameObject> models;
    private int selectionIndext = 0;

   
    void Start () {
        models = new List<GameObject>();
        foreach (Transform t in transform)
        {
            models.Add(t.gameObject);
            t.gameObject.SetActive(false);
        }

        models[selectionIndext].SetActive(true);
    }
	
	// Update is called once per frame
	void Update () {
	
	}
}
