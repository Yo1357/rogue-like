using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;

public class GameController : MonoBehaviour {

	public static GameController Instance;
    public bool isPlayerTurn;
    public bool areEnemiesMoving;
    public int playerCurrentHealth = 51;
    public AudioClip gameOverSound;
    
    public GameObject easy;
    public GameObject hard;
    public GameObject insane;

    private GameObject greenPlayer;
    private int playerID;
    private BoardController boardController;
    private List<Enemy> enemies;
    private GameObject levelImage;
    private Text levelText;
    private GameObject StartingImage;
    private bool settingUpGame;
    private int secondsUntilLevelStart = 2;
    private int currentLevel = 1;
    public GameObject playerRed;
    public GameObject playerGreen;

    public void PlayerRed()
    {
        playerGreen.SetActive(false);
        playerRed.SetActive(true);
        StartingImage = GameObject.Find("Starting Image");
        StartingImage.SetActive(false);
    }

    public void PlayerGreen()
    {
        playerRed.SetActive(false);
        playerGreen.SetActive(true);
        StartingImage = GameObject.Find("Starting Image");
        StartingImage.SetActive(false);
    }

    public void Easy()
    {
        easy.SetActive(true);
    }
    public void Medium()
    {
        GameController.Instance.playerCurrentHealth = 50;
    }
    public void Hard()
    {
        hard.SetActive(true);
    }
    public void Insane()
    {
        insane.SetActive(true);
    }

   void Awake () {
        if(Instance != null && Instance != this)
        {
            DestroyImmediate(gameObject);
            return; 
       }
        
        
        Instance = this;
        DontDestroyOnLoad(gameObject);
        boardController = GetComponent<BoardController>();
        enemies = new List<Enemy>();
    }

	
	void Start()
    {
        InitializeGame();
        
    }
    
    
    public void startButton()
    {
        StartingImage = GameObject.Find("Starting Image");
        StartingImage.SetActive(false);

    } 
    private void InitializeGame()
    {
        
        settingUpGame = true;
        levelImage = GameObject.Find("Level Image");
        levelText = GameObject.Find("Level Text").GetComponent<Text>();
        levelText.text = "Day " + currentLevel;
        levelImage.SetActive(true);
        enemies.Clear();
        boardController.SetupLevel(currentLevel);
        Invoke("DisableLevelImage", secondsUntilLevelStart);
        if(currentLevel > 1)
        {
            StartingImage = GameObject.Find("Starting Image");
            StartingImage.SetActive(false);
            
        }
        
    }

    private void DisableLevelImage()
    {
        levelImage.SetActive(false);
        settingUpGame = false;
        isPlayerTurn = true;
        areEnemiesMoving = false;
    }

    private void OnLevelWasLoaded(int levelLoaded)
    {
        currentLevel++;
        InitializeGame();
        
    }

	void Update () {
        if(isPlayerTurn || areEnemiesMoving || settingUpGame)
        {
            return;
        }
            StartCoroutine(MoveEnemies());
	}

    private IEnumerator MoveEnemies()
    {
        areEnemiesMoving = true;

        yield return new WaitForSeconds(0.2f);

        foreach(Enemy enemy in enemies)
        {
            enemy.MoveEnemy();
            yield return new WaitForSeconds(enemy.moveTime);
        }

        areEnemiesMoving = false;
        isPlayerTurn = true;
    }


    //Adds enemy
    public void AddEnemyToList(Enemy enemy)
    {
        enemies.Add(enemy);
    }

    private bool CheckHighscore()
    {
        if (currentLevel > PlayerPrefs.GetInt("highscore", 0))
        {
            return true;
        }
        else return false;
    }
    private int GetHighscore()
    {
        if ( CheckHighscore() )
        {
            PlayerPrefs.SetInt("highscore", currentLevel);
        }
        return PlayerPrefs.GetInt("highscore", 0); 
    }

    public void GameOver()
    {
        bool brokeHighscore = CheckHighscore();
        int highScore = GetHighscore();
        isPlayerTurn = false;
        SoundController.Instance.music.Stop();
        SoundController.Instance.PlaySingle(gameOverSound);
        if (brokeHighscore)
        {
            levelText.text = "You starved after " + currentLevel + " days....\n" + "Congratulations New Highscore: " + highScore;
        }
        else
        {
            levelText.text = "You starved after " + currentLevel + " days....\n" + "Old HighScore: " + highScore;
        }
        levelImage.SetActive(true);
        enabled = false;
    }
}
