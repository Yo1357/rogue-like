﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class Player : MovingObjects {

    public Text healthText;
    public AudioClip movementSounds1;
    public AudioClip movementSounds2;
    public AudioClip chopSounds1;
    public AudioClip chopSounds2;
    public AudioClip fruitSound1;
    public AudioClip fruitSound2;
    public AudioClip sodaSound1;
    public AudioClip sodaSound2;


    private Animator animator;
    public int playerHealth = 50;
    private int attackPower = 1;
    private int healthPerFruit = 5;
    private int healthPerSoda = 10;
    private int healthPerTorch = 15;
    private int healthPerSprings = 1;
    private int easyHealth = 25;
    private int hardHealth = 25;
    private int insaneHealth = 40;
    private int secondsUntilNextLevel = 1;

    
    protected override void Start()
    {
        base.Start();
        animator = GetComponent<Animator>();
        playerHealth = GameController.Instance.playerCurrentHealth;
        healthText.text = "Health: " + playerHealth;
    }

    private void OnDisable()
    {
        GameController.Instance.playerCurrentHealth = playerHealth;
       
    }

    void Update () {
        
        if (!GameController.Instance.isPlayerTurn)
        {
            return;
        }

        CheckIfGameOver();

        int xAxis = 0;
        int yAxis = 0;
        
        xAxis = (int)Input.GetAxisRaw("Horizontal");
        yAxis = (int)Input.GetAxisRaw("Vertical");

        if(xAxis != 0)
        {
            yAxis = 0;
        }
        if(xAxis != 0 || yAxis != 0)
        {
            playerHealth--;
            healthText.text = "Health: " + playerHealth;
            SoundController.Instance.PlaySingle(movementSounds1, movementSounds2);
            Move<Wall>(xAxis, yAxis);
            GameController.Instance.isPlayerTurn = false;
        }
        
    }

    private void OnTriggerEnter2D(Collider2D objectPLayerCollideWith)
    {
        if(objectPLayerCollideWith.tag == "Exit")
        {
            Invoke("LoadNewLevel", secondsUntilNextLevel);
            enabled = false;
        }
        else if(objectPLayerCollideWith.tag == "Fruit")
        {
            playerHealth += healthPerFruit;
            healthText.text = "+" + healthPerFruit + " Health\n" + playerHealth;
            objectPLayerCollideWith.gameObject.SetActive(false);
            SoundController.Instance.PlaySingle(fruitSound1, fruitSound2);       
        }
        else if(objectPLayerCollideWith.tag == "Soda")
        {
            playerHealth += healthPerSoda;
            healthText.text = "+" + healthPerSoda + " Health\n" + playerHealth;
            objectPLayerCollideWith.gameObject.SetActive(false);
            SoundController.Instance.PlaySingle(sodaSound1, sodaSound2);
        }
        else if(objectPLayerCollideWith.tag == "Torch")
        {
            playerHealth += healthPerTorch;
            healthText.text = "+" + healthPerTorch + " Health\n" + playerHealth;
            objectPLayerCollideWith.gameObject.SetActive(false);
        }
        else if (objectPLayerCollideWith.tag == "Springs")
        {
            playerHealth += healthPerSprings;
            healthText.text = "+" + healthPerSprings + " Health\n" + playerHealth;
        }
        else if (objectPLayerCollideWith.tag == "Easy")
        {
            playerHealth += easyHealth;
            healthText.text = "+" + easyHealth + " Health\n" + playerHealth;
            objectPLayerCollideWith.gameObject.SetActive(false);
        }
        else if (objectPLayerCollideWith.tag == "Hard")
        {
            playerHealth -= hardHealth;
            healthText.text = "-" + hardHealth + " Health\n" + playerHealth;
            objectPLayerCollideWith.gameObject.SetActive(false);
        }
        else if (objectPLayerCollideWith.tag == "Insane")
        {
            playerHealth -= insaneHealth;
            healthText.text = "-" + insaneHealth + " Health\n" + playerHealth;
            objectPLayerCollideWith.gameObject.SetActive(false);
        }
    }

    private void LoadNewLevel()
    {
      //  DontDestroyOnLoad(GameController.Instance.playerGreen);
      //  DontDestroyOnLoad(GameController.Instance.playerRed);

        
        Application.LoadLevel(Application.loadedLevel);
        
    }

    protected override void HadleCollision<T>(T component)
    {
        Wall wall = component as Wall;
        animator.SetTrigger("playerAttack");
        SoundController.Instance.PlaySingle(chopSounds1, chopSounds2);
        wall.DamageWall(attackPower);

    }

    public void TakeDamage(int damageReceived)
    {
        playerHealth -= damageReceived;
        healthText.text = "-" + damageReceived + " Health\n" + "Health: " + playerHealth;
        animator.SetTrigger("playerHurt");
    }

    private void CheckIfGameOver()
    {
        if(playerHealth <= 0)
      {
          GameController.Instance.GameOver();
      }
    }
}
